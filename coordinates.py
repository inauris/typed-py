import json
import ssl
import urllib.request
from json.decoder import JSONDecodeError
from typing import NamedTuple
from urllib.error import URLError

from config import USE_ROUNDED_COORDS
from exceptions import CantGetCoordinates


class Coordinates(NamedTuple):
    latitude: float
    longitude: float


def get_coordinates() -> Coordinates:
    """Returns current coordinates by ip"""
    coordinates_response = _get_coordinates_response()
    coordinates = _parse_coordinates_response(coordinates_response)
    return coordinates


def _get_coordinates_response() -> str:
    ssl._create_default_https_context = ssl._create_unverified_context
    url = "https://ipinfo.io"
    try:
        return urllib.request.urlopen(url).read()
    except URLError:
        raise CantGetCoordinates


def _parse_coordinates_response(coordinates_response: str) -> Coordinates:
    try:
        coordinates_dict = json.loads(coordinates_response)
    except JSONDecodeError:
        raise CantGetCoordinates
    coordinates = Coordinates(
        latitude=_parse_lat(coordinates_dict),
        longitude=_parse_long(coordinates_dict),
    )
    return _round_coordinates(coordinates)


def _parse_lat(coordinates_dict: dict) -> float:
    return float(coordinates_dict["loc"].split(",")[0])


def _parse_long(coordinates_dict: dict) -> float:
    return float(coordinates_dict["loc"].split(",")[1])


def _round_coordinates(coordinates: Coordinates) -> Coordinates:
    if not USE_ROUNDED_COORDS:
        return coordinates
    return Coordinates(
        *map(
            lambda c: round(c, 1),
            [coordinates.latitude, coordinates.longitude],
        )
    )


if __name__ == "__main__":
    print(get_coordinates())
