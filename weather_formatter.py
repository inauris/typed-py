from weather_api_service import Weather


def format_weather(weather: Weather) -> str:
    """Formats weather data in string"""
    return (
        f"{weather.city}, температура {weather.temperature} °C, "
        f"{weather.weather_condition.value}\n"
        f"Восход: {weather.sunrise.strftime('%H:%M')}\n"
        f"Закат: {weather.sunset.strftime('%H:%M')}\n"
    )


if __name__ == "__main__":
    from datetime import datetime
    from weather_api_service import WeatherCondition

    print(
        format_weather(
            Weather(
                temperature=20,
                weather_condition=WeatherCondition.CLEAR,
                sunrise=datetime.fromisoformat("2022-05-04 04:00:00"),
                sunset=datetime.fromisoformat("2022-05-04 20:25:00"),
                city="Karaganda",
            )
        )
    )
