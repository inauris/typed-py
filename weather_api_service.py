import json
import ssl
import urllib.request
from datetime import datetime
from enum import Enum
from json.decoder import JSONDecodeError
from typing import Literal, NamedTuple
from urllib.error import URLError

from config import OPENWEATHER_URL_TEMPLATE
from coordinates import Coordinates
from exceptions import ApiServiceError

Celsius = int


class WeatherCondition(Enum):
    THUNDERSTORM = "Гроза"
    DRIZZLE = "Изморозь"
    RAIN = "Дождь"
    SNOW = "Снег"
    CLEAR = "Ясно"
    FOG = "Туман"
    CLOUDS = "Облачно"


class Weather(NamedTuple):
    temperature: Celsius
    weather_condition: WeatherCondition
    sunrise: datetime
    sunset: datetime
    city: str


def get_weather(coordinates: Coordinates) -> Weather:
    """Requests weather in OpenWeather API and returns it"""
    openweather_response = _get_openweather_response(
        latitude=coordinates.latitude, longitude=coordinates.longitude
    )
    weather = _parse_openweather_response(openweather_response)
    return weather


def _get_openweather_response(latitude: float, longitude: float) -> str:
    ssl._create_default_https_context = ssl._create_unverified_context
    url = OPENWEATHER_URL_TEMPLATE.format(
        latitude=latitude, longitude=longitude
    )
    try:
        return urllib.request.urlopen(url).read()
    except URLError:
        raise ApiServiceError


def _parse_openweather_response(openweather_response: str) -> Weather:
    try:
        openweather_dict = json.loads(openweather_response)
    except JSONDecodeError:
        raise ApiServiceError
    return Weather(
        temperature=_parse_temperature(openweather_dict),
        weather_condition=_parse_weather_condition(openweather_dict),
        sunrise=_parse_sun_time(openweather_dict, "sunrise"),
        sunset=_parse_sun_time(openweather_dict, "sunset"),
        city=_parse_city(openweather_dict),
    )


def _parse_temperature(openweather_dict: dict) -> Celsius:
    return round(openweather_dict["main"]["temp"])


def _parse_weather_condition(openweather_dict: dict) -> WeatherCondition:
    try:
        weather_condition_id = str(openweather_dict["weather"][0]["id"])
    except (IndexError, KeyError):
        raise ApiServiceError
    weather_conditions = {
        "1": WeatherCondition.THUNDERSTORM,
        "3": WeatherCondition.DRIZZLE,
        "5": WeatherCondition.RAIN,
        "6": WeatherCondition.SNOW,
        "7": WeatherCondition.FOG,
        "800": WeatherCondition.CLEAR,
        "80": WeatherCondition.CLOUDS,
    }

    for _id, _weather_condition in weather_conditions.items():
        if weather_condition_id.startswith(_id):
            return _weather_condition
    raise ApiServiceError


def _parse_sun_time(
    openweather_dict: dict, time: Literal["sunrise", "sunset"]
) -> datetime:
    return datetime.fromtimestamp(openweather_dict["sys"][time])


def _parse_city(openweather_dict: dict) -> str:
    return str(openweather_dict["name"])


if __name__ == "__main__":
    from weather_formatter import format_weather

    print(
        format_weather(get_weather(Coordinates(latitude=55.7, longitude=37.6)))
    )
