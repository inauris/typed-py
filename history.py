import json
from datetime import datetime
from pathlib import Path
from typing import TypedDict

from weather_api_service import Weather
from weather_formatter import format_weather


class WeatherStorage:
    """Interface for any storage saving weather"""

    def save(self, weather: Weather) -> None:
        raise NotImplementedError


class StoredWeather(TypedDict):
    date: str
    weather: str


class JSONFileWeatherStorage(WeatherStorage):
    """Store weather in JSON file"""

    def __init__(self):
        self._init_strorage()

    _jsonfile = Path(__file__).resolve().parent / "history.json"

    def save(self, weather: Weather) -> None:
        history = self._read_history()
        history.append(
            StoredWeather(
                {
                    "date": str(datetime.now()),
                    "weather": format_weather(weather),
                }
            )
        )
        self._write(history)

    def _init_strorage(self) -> None:
        if not self._jsonfile.exists():
            self._jsonfile.write_text("[]")

    def _read_history(self) -> list[StoredWeather]:
        with open(self._jsonfile, "r") as f:
            return json.load(f)

    def _write(self, history: list[StoredWeather]) -> None:
        with open(self._jsonfile, "w") as f:
            json.dump(history, f, ensure_ascii=False, indent=4)


class PlainFileWeatherStorage(WeatherStorage):
    """Store weather in plain text file"""

    _txtfile = Path(__file__).resolve().parent / "history.txt"

    def save(self, weather: Weather) -> None:
        now = datetime.now()
        formatted_weather = format_weather(weather)
        with open(self._txtfile, "a") as f:
            f.write(f"{now}\n{formatted_weather}\n")


def save_weather(weather: Weather, storage: WeatherStorage) -> None:
    """Saves weather in the storage"""
    storage.save(weather)
