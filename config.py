USE_ROUNDED_COORDS = False
OPENWEATHER_APIKEY = "cb20a693170a4cfa0e6093f61244dcff"
OPENWEATHER_URL_TEMPLATE = (
    "https://api.openweathermap.org/data/2.5/weather?"
    "lat={latitude}&lon={longitude}&"
    "appid=" + OPENWEATHER_APIKEY + "&units=metric"
)
